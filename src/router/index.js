import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: () => import("../views/HomeView.vue"),
    },
    {
      path: "/Accessories",
      name: "accessories",
      component: () => import("../views/AccessoriesView.vue"),
    },

    {
      path: "/Appleiphone",
      name: "appleiphone",
      component: () => import("../views/AllProduct/AppleIphone.vue"),
    },
    {
      path: "/Xiaomi",
      name: "xiaomi",
      component: () => import("../views/AllProduct/XiaoMi.vue"),
    },
    {
      path: "/Vivo",
      name: "vivo",
      component: () => import("../views/AllProduct/ViVo.vue"),
    },
    {
      path: "/Oppo",
      name: "oppo",
      component: () => import("../views/AllProduct/OpPo.vue"),
    },
    {
      path: "/signin",
      name: "signin",
      component: () => import("../views/Sign In/SignIn.vue"),
    },
    {
      path: "/signup",
      name: "signup",
      component: () => import("../views/Sign Up/SignUp.vue"),
    },
  ],
});

export default router;
